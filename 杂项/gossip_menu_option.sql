-- 刀锋山:托什雷的基地 波西(creature:21112),gossip_menu_option:8661
DELETE FROM `gossip_menu_option` WHERE (`menu_id`=8661 AND `id`=0);
INSERT INTO `gossip_menu_option` (`menu_id`, `id`, `option_icon`, `option_text`, `option_broadcast_text`, `option_id`, `npc_option_npcflag`, `action_menu_id`, `action_poi_id`, `action_script_id`, `box_coded`, `box_money`, `box_text`, `box_broadcast_text`, `condition_id`) VALUES ('8661', '0', '1', 'Show me your assortment of, err... things.', '21285', '3', '128', '0', '0', '0', '0', '0', '', '0', '0');
DELETE FROM `locales_gossip_menu_option` WHERE (`menu_id`=8661 AND `id`=0);
INSERT INTO `locales_gossip_menu_option` (`menu_id`, `id`, `option_text_loc4`, `box_text_loc4`) VALUES (8661, 0, '让我看看你都有些什么……呃，东西……', '');
-- 暴风城公会接待中心 瑞贝卡(5193),gossip_menu_option:10392
DELETE FROM `gossip_menu_option` WHERE (`menu_id`=10392 AND `id`=0)or(`menu_id`=10392 AND `id`=1);
INSERT INTO `gossip_menu_option` (`menu_id`, `id`, `option_icon`, `option_text`, `option_broadcast_text`, `option_id`, `npc_option_npcflag`, `action_menu_id`, `action_poi_id`, `action_script_id`, `box_coded`, `box_money`, `box_text`, `box_broadcast_text`, `condition_id`) VALUES ('10392', '0', '1', 'I want to browse your goods.', '3370', '3', '128', '0', '0', '0', '0', '0', '', '0', '0');
INSERT INTO `gossip_menu_option` (`menu_id`, `id`, `option_icon`, `option_text`, `option_broadcast_text`, `option_id`, `npc_option_npcflag`, `action_menu_id`, `action_poi_id`, `action_script_id`, `box_coded`, `box_money`, `box_text`, `box_broadcast_text`, `condition_id`) VALUES ('10392', '1', '8', 'I want to create a guild crest.', '3415', '11', '524288', '0', '0', '0', '0', '0', '', '0', '0');
DELETE FROM `locales_gossip_menu_option` WHERE (`menu_id`=10392 AND `id`=0)or(`menu_id`=10392 AND `id`=1);
INSERT INTO `locales_gossip_menu_option` (`menu_id`, `id`, `option_text_loc4`, `box_text_loc4`) VALUES (10392, 0, '我想要看看你卖的货物。', '');
INSERT INTO `locales_gossip_menu_option` (`menu_id`, `id`, `option_text_loc4`, `box_text_loc4`) VALUES (10392, 1, '我要设计一件公会战袍。', '');
-- 暴风城公会接待中心 奥德文(4974),gossip_menu_option:708
DELETE FROM `gossip_menu_option` WHERE (`menu_id`=708 AND `id`=0)or(`menu_id`=708 AND `id`=1);
INSERT INTO `gossip_menu_option` (`menu_id`, `id`, `option_icon`, `option_text`, `option_broadcast_text`, `option_id`, `npc_option_npcflag`, `action_menu_id`, `action_poi_id`, `action_script_id`, `box_coded`, `box_money`, `box_text`, `box_broadcast_text`, `condition_id`) VALUES ('708', '0', '7', 'How do I form a guild?', '0', '10', '262144', '0', '0', '0', '0', '0', '', '0', '0');
INSERT INTO `gossip_menu_option` (`menu_id`, `id`, `option_icon`, `option_text`, `option_broadcast_text`, `option_id`, `npc_option_npcflag`, `action_menu_id`, `action_poi_id`, `action_script_id`, `box_coded`, `box_money`, `box_text`, `box_broadcast_text`, `condition_id`) VALUES ('708', '1', '8', 'I want to create a guild crest.', '0', '11', '524288', '0', '0', '0', '0', '0', '', '0', '0');
DELETE FROM `locales_gossip_menu_option` WHERE (`menu_id`=708 AND `id`=0)or(`menu_id`=708 AND `id`=1);
INSERT INTO `locales_gossip_menu_option` (`menu_id`, `id`, `option_text_loc4`, `box_text_loc4`) VALUES (708, 0, '我如何才能建立一个公会？', '');
INSERT INTO `locales_gossip_menu_option` (`menu_id`, `id`, `option_text_loc4`, `box_text_loc4`) VALUES (708, 1, '我要设计一件公会战袍。', '');
-- 银月城公会接待中心 坦德莉恩(16568),gossip_menu_option:7440
DELETE FROM `gossip_menu_option` WHERE (`menu_id`=7440 AND `id`=0)or(`menu_id`=7440 AND `id`=1);
INSERT INTO `gossip_menu_option` (`menu_id`, `id`, `option_icon`, `option_text`, `option_broadcast_text`, `option_id`, `npc_option_npcflag`, `action_menu_id`, `action_poi_id`, `action_script_id`, `box_coded`, `box_money`, `box_text`, `box_broadcast_text`, `condition_id`) VALUES ('7440', '0', '7', 'How do I form a guild?', '0', '10', '262144', '0', '0', '0', '0', '0', '', '0', '0');
INSERT INTO `gossip_menu_option` (`menu_id`, `id`, `option_icon`, `option_text`, `option_broadcast_text`, `option_id`, `npc_option_npcflag`, `action_menu_id`, `action_poi_id`, `action_script_id`, `box_coded`, `box_money`, `box_text`, `box_broadcast_text`, `condition_id`) VALUES ('7440', '1', '8', 'I want to create a guild crest.', '0', '11', '524288', '0', '0', '0', '0', '0', '', '0', '0');
DELETE FROM `locales_gossip_menu_option` WHERE (`menu_id`=7440 AND `id`=0)or(`menu_id`=7440 AND `id`=1);
INSERT INTO `locales_gossip_menu_option` (`menu_id`, `id`, `option_text_loc4`, `box_text_loc4`) VALUES (7440, 0, '我如何才能建立一个公会？', '');
INSERT INTO `locales_gossip_menu_option` (`menu_id`, `id`, `option_text_loc4`, `box_text_loc4`) VALUES (7440, 1, '我要设计一件公会战袍。', '');
-- 银月城法师传送门训练师(16654),gossip_menu_option:4824
-- DELETE FROM `gossip_menu_option` WHERE (`menu_id`=4824 AND `id`=0)or(`menu_id`=4824 AND `id`=1);
-- 埃索达骑术训练师(20914),gossip_menu_option:8553
DELETE FROM `gossip_menu_option` WHERE (`menu_id`=8553 AND `id`=0);
INSERT INTO `gossip_menu_option` (`menu_id`, `id`, `option_icon`, `option_text`, `option_broadcast_text`, `option_id`, `npc_option_npcflag`, `action_menu_id`, `action_poi_id`, `action_script_id`, `box_coded`, `box_money`, `box_text`, `box_broadcast_text`, `condition_id`) VALUES ('8553', '0', '3', 'I seek training to ride a steed.', '0', '5', '16', '0', '0', '0', '0', '0', '', '0', '0');
DELETE FROM `locales_gossip_menu_option` WHERE (`menu_id`=8553 AND `id`=0);
INSERT INTO `locales_gossip_menu_option` (`menu_id`, `id`, `option_text_loc4`, `box_text_loc4`) VALUES (8553, 0, '我想接受骑术训练。', '');
--游侠萨琳娜(猎人训练师)(15513),gossip_menu_option:6652
DELETE FROM `gossip_menu_option` WHERE (`menu_id`=6652 AND `id`=0)or(`menu_id`=6652 AND `id`=1);
INSERT INTO `gossip_menu_option` (`menu_id`, `id`, `option_icon`, `option_text`, `option_broadcast_text`, `option_id`, `npc_option_npcflag`, `action_menu_id`, `action_poi_id`, `action_script_id`, `box_coded`, `box_money`, `box_text`, `box_broadcast_text`, `condition_id`) VALUES ('6652', '0', '3', 'I seek training in the ways of the Hunter.', '7643', '5', '16', '0', '0', '0', '0', '0', '', '0', '0');
INSERT INTO `gossip_menu_option` (`menu_id`, `id`, `option_icon`, `option_text`, `option_broadcast_text`, `option_id`, `npc_option_npcflag`, `action_menu_id`, `action_poi_id`, `action_script_id`, `box_coded`, `box_money`, `box_text`, `box_broadcast_text`, `condition_id`) VALUES ('6652', '1', '0', 'I wish to unlearn my talents.', '62295', '1', '1', '4461', '0', '0', '0', '0', '', '0', '135');
DELETE FROM `locales_gossip_menu_option` WHERE (`menu_id`=6652 AND `id`=0)or(`menu_id`=6652 AND `id`=1);
INSERT INTO `locales_gossip_menu_option` (`menu_id`, `id`, `option_text_loc4`, `box_text_loc4`) VALUES (6652, 0, '我想接受成为猎人的训练。', '');
INSERT INTO `locales_gossip_menu_option` (`menu_id`, `id`, `option_text_loc4`, `box_text_loc4`) VALUES (6652, 1, '我想要遗忘我的天赋。', '');