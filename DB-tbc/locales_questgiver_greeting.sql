-- ===========================================
-- 清理 locales_questgiver_greeting
-- ===========================================
TRUNCATE TABLE locales_questgiver_greeting;


-- ===========================================
-- 插入 locales_questgiver_greeting 数据
-- ===========================================

INSERT INTO `locales_questgiver_greeting` (`Entry`, `Type`, `Text_loc4`) VALUES (234, 0, '西部荒野已经被黑暗的势力笼罩了。当我在洛丹伦的战场上执行任务的时候，这些农场被暴徒和杀人犯占领，成了他们的乐园。人民军需要你的帮助。');
INSERT INTO `locales_questgiver_greeting` (`Entry`, `Type`, `Text_loc4`) VALUES (235, 0, '欢迎光临寒舍！能看到一个友善的人类总是件令人愉快的事，你看起来真强壮！我和我丈夫总在农场周围寻找帮助，大多数好心人都走了，因此要找到一个真正有能力帮助我们的人很不容易。');
INSERT INTO `locales_questgiver_greeting` (`Entry`, `Type`, `Text_loc4`) VALUES (237, 0, '这一带真是兵荒马乱，我想要说服愚蠢的萨丁赶快离开这里，可他就是不听。我可没他那么傻，等弗娜备好马车，我就会赶紧逃跑。');
INSERT INTO `locales_questgiver_greeting` (`Entry`, `Type`, `Text_loc4`) VALUES (238, 0, '有时候我认为天空中有那么一大片乌云，专门将厄运像下雨一样洒在我们身上。起初，我们被迫离开自己的土地，现在我们甚至又逃离不出西部荒野。什么事都是一团糟，真不知道该怎么办了！');
INSERT INTO `locales_questgiver_greeting` (`Entry`, `Type`, `Text_loc4`) VALUES (240, 0, '啊，就算没有这些新麻烦不断的冒出来，维持这儿的秩序就已经够让人头痛的了！希望你有好消息，$n……');
INSERT INTO `locales_questgiver_greeting` (`Entry`, `Type`, `Text_loc4`) VALUES (241, 0, '嘿,朋友。我叫雷米。我是从东部的赤脊山来的，想来这里找些事做，找些事做。你们有…有吗??');
INSERT INTO `locales_questgiver_greeting` (`Entry`, `Type`, `Text_loc4`) VALUES (253, 0, '啊！好天气，好天气啊，$C！来吧，坐下喝一杯。我从你的眼里看到了无比的勇气，和我聊天是不会让你后悔的……');
INSERT INTO `locales_questgiver_greeting` (`Entry`, `Type`, `Text_loc4`) VALUES (261, 0, '你好啊。你看上去心事重重的，找暴风城军队有什么事吗？');
INSERT INTO `locales_questgiver_greeting` (`Entry`, `Type`, `Text_loc4`) VALUES (264, 0, '放松些，$c。如果你只是过路的，我建议你坚持走大路，并且只在白天赶路。如果你在夜色镇做生意，不如考虑一下和守夜人合伙。我们的实力不容置疑，只是人手少了些。');
INSERT INTO `locales_questgiver_greeting` (`Entry`, `Type`, `Text_loc4`) VALUES (265, 0, '我早就已经预感到你要来了，$n，星象已经很明确地告诉了我。');
INSERT INTO `locales_questgiver_greeting` (`Entry`, `Type`, `Text_loc4`) VALUES (272, 0, '你好，你好！欢迎来到我的厨房，$g 先生 : 女士;！血鸦旅店里最美味的食品都来自这里。啊，来闻一下这香味吧！');
INSERT INTO `locales_questgiver_greeting` (`Entry`, `Type`, `Text_loc4`) VALUES (273, 0, '把门关紧了，$c。鬼才知道那帮强盗什么时候会冲进镇子里来。');
INSERT INTO `locales_questgiver_greeting` (`Entry`, `Type`, `Text_loc4`) VALUES (341, 0, '我没时间和你聊天，$N。在大雨来临之前我得重建这座桥。我已经按时完成了每个阶段的工程并且现在还在预算范围之内，但现在还不是休息的时候。');
INSERT INTO `locales_questgiver_greeting` (`Entry`, `Type`, `Text_loc4`) VALUES (342, 0, '向您致敬，$n！欢迎来到我这简陋的花园。最近天气很好，希望在收割之前一直都能这么好。');
INSERT INTO `locales_questgiver_greeting` (`Entry`, `Type`, `Text_loc4`) VALUES (344, 0, '这个站在暴风王国湖畔镇议事厅的$C是谁？说你呢，来这里干什么，人类？兽人对这个王国的威胁太大了，我们没有时间聊天。');
INSERT INTO `locales_questgiver_greeting` (`Entry`, `Type`, `Text_loc4`) VALUES (381, 0, '你的到来让我很高兴，$c。如果你来这里做生意，那么就倒杯酒，咱们谈谈。');
INSERT INTO `locales_questgiver_greeting` (`Entry`, `Type`, `Text_loc4`) VALUES (382, 0, '我没有时间跟你聊天，但如果你愿意帮我们打败兽人，那就太好了。');
INSERT INTO `locales_questgiver_greeting` (`Entry`, `Type`, `Text_loc4`) VALUES (392, 0, '不要害怕，$r。我很早以前就死了。但我对你们一直没有恶意。我一生中目睹了太多的死亡。或许你能对我的事业有所帮助。');
INSERT INTO `locales_questgiver_greeting` (`Entry`, `Type`, `Text_loc4`) VALUES (415, 0, '嘿，伙计，你能帮我做件事吗？我现在遇到一点麻烦……');
INSERT INTO `locales_questgiver_greeting` (`Entry`, `Type`, `Text_loc4`) VALUES (633, 0, '在已经到来的黑暗时代，$c……我们马上就会失去一切……到那时，光明只会眷顾那些真正遵循光明之道的人。');
INSERT INTO `locales_questgiver_greeting` (`Entry`, `Type`, `Text_loc4`) VALUES (656, 0, '到处都是盗贼！$B$B窑洞坍塌了。我想所有矿工都死了，包括我的兄弟，他是工头。');
INSERT INTO `locales_questgiver_greeting` (`Entry`, `Type`, `Text_loc4`) VALUES (733, 0, '在这附近行走要小心，$g小伙子:小姑娘;。你可能不是我们这个团队的，但我不会因此而见死不救。');
INSERT INTO `locales_questgiver_greeting` (`Entry`, `Type`, `Text_loc4`) VALUES (773, 0, '$C，嗯？我是克拉兹克，大财主里维加兹的秘书。我知道丛林中发生的所有事情。或许你对棘齿城的油价感兴趣？不感兴趣？那么你是来找工作的？我可以帮你。');
INSERT INTO `locales_questgiver_greeting` (`Entry`, `Type`, `Text_loc4`) VALUES (823, 0, '$c，你好。通常我应该在巡逻，保卫暴风城的人民。但现在许多暴风城的卫兵都在其他地区作战。所以我来了这里，代表他们来保卫边疆，虽然我更希望自己在巡逻……');
INSERT INTO `locales_questgiver_greeting` (`Entry`, `Type`, `Text_loc4`) VALUES (1239, 0, '如果你能忍受让你浑身发抖，衣衫浸湿的故事，那么过来喝一杯，坐一会儿……');
INSERT INTO `locales_questgiver_greeting` (`Entry`, `Type`, `Text_loc4`) VALUES (1719, 0, '过来，你这个没用的东西……！如果你想做点有用的事情，快过来听好了！');
INSERT INTO `locales_questgiver_greeting` (`Entry`, `Type`, `Text_loc4`) VALUES (2263, 0, '我希望你来这里是为了找工作的，$C。我有很多事情要做，部落、辛迪加和食人魔都不打算帮我们。');
INSERT INTO `locales_questgiver_greeting` (`Entry`, `Type`, `Text_loc4`) VALUES (2276, 0, '当我被授予南海镇镇长的职位的时候，我感到特别高兴，因为当时我还十分的年轻。$B$B但现在我在这里，我想知道如果我在暴风城整理文件会不会状况更好些。');
INSERT INTO `locales_questgiver_greeting` (`Entry`, `Type`, `Text_loc4`) VALUES (2700, 0, '我们在避难谷地这里捍卫着阿拉希高地激流堡仅存的一小块土地，我们正在节节败退……$B$B如果你有消息告诉我，希望是好消息。');
INSERT INTO `locales_questgiver_greeting` (`Entry`, `Type`, `Text_loc4`) VALUES (2817, 0, '在荒芜之地流浪的你一定很贫困，$c。就像我一样贫困。$B$B或者你来这里就是因为你疯了。像我一样疯。');
INSERT INTO `locales_questgiver_greeting` (`Entry`, `Type`, `Text_loc4`) VALUES (2860, 0, '其实，我们这一次的出行非常匆忙，$c。我们把没有固定的或者可以移动的任何东西都匆匆带上了。所以我们带的东西不少，补给还有一些剩余。');
INSERT INTO `locales_questgiver_greeting` (`Entry`, `Type`, `Text_loc4`) VALUES (3446, 0, '是的是的是的！你就是我要找的$r！$B$B坐下！我们有很多话要谈！！');
INSERT INTO `locales_questgiver_greeting` (`Entry`, `Type`, `Text_loc4`) VALUES (3649, 0, '黑暗的力量向我们的边界靠近，远古的邪恶重新显现，新的恶魔出现，破坏土地微妙的平衡。在这样黑暗的时代，我们都必须警惕。');
INSERT INTO `locales_questgiver_greeting` (`Entry`, `Type`, `Text_loc4`) VALUES (3666, 0, '嗯……我可以把线插在这里，这样可以驱动菲兹坦型齿轮，但那时水压驱动器就需要候补能量来源……或许我可以……哦，你好！嘿，想帮我一起搞发明吗？');
INSERT INTO `locales_questgiver_greeting` (`Entry`, `Type`, `Text_loc4`) VALUES (3995, 0, '靈魂很不安!');
INSERT INTO `locales_questgiver_greeting` (`Entry`, `Type`, `Text_loc4`) VALUES (4498, 0, '你好，$c。');
INSERT INTO `locales_questgiver_greeting` (`Entry`, `Type`, `Text_loc4`) VALUES (6579, 0, '为了诺姆瑞根！');
INSERT INTO `locales_questgiver_greeting` (`Entry`, `Type`, `Text_loc4`) VALUES (9562, 0, '你好，$R。');