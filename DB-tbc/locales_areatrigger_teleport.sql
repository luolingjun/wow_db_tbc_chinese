-- ===========================================
-- 清理 locales_areatrigger_teleport
-- ===========================================
TRUNCATE TABLE locales_areatrigger_teleport;


-- ===========================================
-- 插入 locales_areatrigger_teleport 数据
-- ===========================================

INSERT INTO `locales_areatrigger_teleport` (`Entry`, `Text_loc4`) VALUES (45, '你必须至少达到 20 级才能进入。');
INSERT INTO `locales_areatrigger_teleport` (`Entry`, `Text_loc4`) VALUES (78, '你必须至少达到 10 级才能进入。');
INSERT INTO `locales_areatrigger_teleport` (`Entry`, `Text_loc4`) VALUES (101, '你必须至少达到 15 级才能进入。');
INSERT INTO `locales_areatrigger_teleport` (`Entry`, `Text_loc4`) VALUES (145, '你必须至少达到 10 级才能进入。');
INSERT INTO `locales_areatrigger_teleport` (`Entry`, `Text_loc4`) VALUES (228, '你必须至少达到 10 级才能进入。');
INSERT INTO `locales_areatrigger_teleport` (`Entry`, `Text_loc4`) VALUES (244, '你必须至少达到 15 级才能进入。');
INSERT INTO `locales_areatrigger_teleport` (`Entry`, `Text_loc4`) VALUES (257, '你必须至少达到 10 级才能进入。');
INSERT INTO `locales_areatrigger_teleport` (`Entry`, `Text_loc4`) VALUES (286, '你必须至少达到 30 级才能进入。');
INSERT INTO `locales_areatrigger_teleport` (`Entry`, `Text_loc4`) VALUES (324, '你必须至少达到 15 级才能进入。');
INSERT INTO `locales_areatrigger_teleport` (`Entry`, `Text_loc4`) VALUES (442, '你必须至少达到 25 级才能进入。');
INSERT INTO `locales_areatrigger_teleport` (`Entry`, `Text_loc4`) VALUES (446, '你必须至少达到 35 级才能进入。');
INSERT INTO `locales_areatrigger_teleport` (`Entry`, `Text_loc4`) VALUES (523, '你必须至少达到 15 级才能进入。');
INSERT INTO `locales_areatrigger_teleport` (`Entry`, `Text_loc4`) VALUES (610, '你必须至少达到 20 级才能进入。');
INSERT INTO `locales_areatrigger_teleport` (`Entry`, `Text_loc4`) VALUES (612, '你必须至少达到 21 级才能进入。');
INSERT INTO `locales_areatrigger_teleport` (`Entry`, `Text_loc4`) VALUES (614, '你必须至少达到 22 级才能进入。');
INSERT INTO `locales_areatrigger_teleport` (`Entry`, `Text_loc4`) VALUES (902, '你必须至少达到 30 级才能进入。');
INSERT INTO `locales_areatrigger_teleport` (`Entry`, `Text_loc4`) VALUES (924, '你必须至少达到 35 级才能进入。');
INSERT INTO `locales_areatrigger_teleport` (`Entry`, `Text_loc4`) VALUES (1466, '你必须至少达到 40 级才能进入。');
INSERT INTO `locales_areatrigger_teleport` (`Entry`, `Text_loc4`) VALUES (1468, '你必须至少达到 45 级才能进入。');
INSERT INTO `locales_areatrigger_teleport` (`Entry`, `Text_loc4`) VALUES (2214, '你必须至少达到 45 级才能进入。');
INSERT INTO `locales_areatrigger_teleport` (`Entry`, `Text_loc4`) VALUES (2216, '你必须至少达到 45 级才能进入。');
INSERT INTO `locales_areatrigger_teleport` (`Entry`, `Text_loc4`) VALUES (2217, '你必须至少达到 45 级才能进入。');
INSERT INTO `locales_areatrigger_teleport` (`Entry`, `Text_loc4`) VALUES (2230, '你必须至少达到 8 级才能进入。');
INSERT INTO `locales_areatrigger_teleport` (`Entry`, `Text_loc4`) VALUES (2527, 'You must be a Stone Guard or higher rank in order to enter the Hall of Legends.');
INSERT INTO `locales_areatrigger_teleport` (`Entry`, `Text_loc4`) VALUES (2532, 'You must be a Knight or higher rank in order to enter the Champions\' Hall.');
INSERT INTO `locales_areatrigger_teleport` (`Entry`, `Text_loc4`) VALUES (2567, '你必须至少达到 45 级才能进入。');
INSERT INTO `locales_areatrigger_teleport` (`Entry`, `Text_loc4`) VALUES (2606, '只有部落可以使用这个传送门。');
INSERT INTO `locales_areatrigger_teleport` (`Entry`, `Text_loc4`) VALUES (2608, '只有联盟可以使用这个传送门。');
INSERT INTO `locales_areatrigger_teleport` (`Entry`, `Text_loc4`) VALUES (2848, 'You must be part of a Raid group, at least level 50 and have the Drakefire Amulet in your inventory to enter.');
INSERT INTO `locales_areatrigger_teleport` (`Entry`, `Text_loc4`) VALUES (2886, 'You must be part of a Raid group and at least level 50 to enter.');
INSERT INTO `locales_areatrigger_teleport` (`Entry`, `Text_loc4`) VALUES (3133, '你必须至少达到 35 级才能进入。');
INSERT INTO `locales_areatrigger_teleport` (`Entry`, `Text_loc4`) VALUES (3134, '你必须至少达到 35 级才能进入。');
INSERT INTO `locales_areatrigger_teleport` (`Entry`, `Text_loc4`) VALUES (3183, '你必须至少达到 45 级才能进入。');
INSERT INTO `locales_areatrigger_teleport` (`Entry`, `Text_loc4`) VALUES (3184, '你必须至少达到 45 级才能进入。');
INSERT INTO `locales_areatrigger_teleport` (`Entry`, `Text_loc4`) VALUES (3185, '你必须至少达到 45 级才能进入。');
INSERT INTO `locales_areatrigger_teleport` (`Entry`, `Text_loc4`) VALUES (3186, '你必须至少达到 45 级才能进入。');
INSERT INTO `locales_areatrigger_teleport` (`Entry`, `Text_loc4`) VALUES (3187, '你必须至少达到 45 级才能进入。');
INSERT INTO `locales_areatrigger_teleport` (`Entry`, `Text_loc4`) VALUES (3189, '你必须至少达到 45 级才能进入。');
INSERT INTO `locales_areatrigger_teleport` (`Entry`, `Text_loc4`) VALUES (3528, 'You must be attuned and in a raid group to enter.');
INSERT INTO `locales_areatrigger_teleport` (`Entry`, `Text_loc4`) VALUES (3726, 'You must be part of a raid group and at least 50th level to enter.');
INSERT INTO `locales_areatrigger_teleport` (`Entry`, `Text_loc4`) VALUES (3928, '您必须在一个团队中并且至少 50 级才能进入。');
INSERT INTO `locales_areatrigger_teleport` (`Entry`, `Text_loc4`) VALUES (3948, '只有联盟可以使用这个传送门。');
INSERT INTO `locales_areatrigger_teleport` (`Entry`, `Text_loc4`) VALUES (3949, '只有部落可以使用这个传送门。');
INSERT INTO `locales_areatrigger_teleport` (`Entry`, `Text_loc4`) VALUES (4008, '除非安其拉的大门已经打开，你不能进入这里，你至少 50 级以上，并且在一个团队中。');
INSERT INTO `locales_areatrigger_teleport` (`Entry`, `Text_loc4`) VALUES (4010, '除非安其拉的大门已经打开，你不能进入这里，你至少 50 级以上，并且在一个团队中。');
INSERT INTO `locales_areatrigger_teleport` (`Entry`, `Text_loc4`) VALUES (4055, '您必须达到 51 级以上，在一个团队中，并且与纳克萨玛斯协调才能进入。');
INSERT INTO `locales_areatrigger_teleport` (`Entry`, `Text_loc4`) VALUES (4320, '你需要完成黑色沼泽系列任务，并安装《燃烧的远征》。');
INSERT INTO `locales_areatrigger_teleport` (`Entry`, `Text_loc4`) VALUES (4321, '你需要完成往日的希尔斯布莱德系列任务，并安装《燃烧的远征》。');
